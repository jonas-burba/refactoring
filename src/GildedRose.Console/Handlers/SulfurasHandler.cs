﻿using GildedRose.Entity;

namespace GildedRose.Console.Handlers
{
    public class SulfurasHandler : BaseHandler
    {
        public SulfurasHandler(BaseHandler nextHandler) : base(nextHandler)
        {
        }

        protected override string Name => "Sulfuras, Hand of Ragnaros";

        public override void ChangeQualityAndSellIn(Item item)
        {
            if (ShouldHandle(item))
            {
                return;
            }
            _nextHandler.ChangeQualityAndSellIn(item);
        }
    }
}
