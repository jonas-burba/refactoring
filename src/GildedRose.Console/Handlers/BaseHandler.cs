﻿using GildedRose.Entity;

namespace GildedRose.Console.Handlers
{
    public class BaseHandler
    {
        protected readonly BaseHandler _nextHandler;
        public BaseHandler(BaseHandler nextHandler)
        {
            _nextHandler = nextHandler;
        }

        protected virtual string Name => null;
        protected virtual int BaseQualityChangeRate => -1;

        protected virtual int QualityChangeRate(int sellIn)
        {
            return sellIn < 0 ? 2 * BaseQualityChangeRate : BaseQualityChangeRate;
        }
        public virtual void ChangeQualityAndSellIn(Item item)
        {
            if (ShouldHandle(item))
            {
                item.Quality += QualityChangeRate(item.SellIn);
                item.Quality = item.Quality <= 0 ? 0 : item.Quality;
                item.Quality = item.Quality >= 50 ? 50 : item.Quality;
                item.SellIn -= 1;
                return;
            }

            _nextHandler?.ChangeQualityAndSellIn(item);
        }
        protected virtual bool ShouldHandle(Item item)
        {
            return string.IsNullOrEmpty(Name) || (!string.IsNullOrEmpty(Name) && Name == item.Name);
        }
    }
}
