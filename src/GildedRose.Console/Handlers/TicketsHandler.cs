﻿using GildedRose.Entity;

namespace GildedRose.Console.Handlers
{
    public class TicketsHandler : BaseHandler
    {
        public TicketsHandler(BaseHandler nextHandler) : base(nextHandler)
        {
        }

        protected override string Name => "Backstage passes to ";
        protected override int BaseQualityChangeRate => 1;

        protected override int QualityChangeRate(int sellIn)
        {
            if (sellIn <= 10)
            {
                return sellIn <= 5 ? 3 : 2;
            }
            return BaseQualityChangeRate;
        }
        public override void ChangeQualityAndSellIn(Item item)
        {
            if (ShouldHandle(item))
            {
                if (item.SellIn > 0)
                {
                    base.ChangeQualityAndSellIn(item);
                }
                else
                {
                    item.SellIn -= 1;
                    item.Quality = 0;
                }
            }
            else
            {
                _nextHandler.ChangeQualityAndSellIn(item);
            }
            
        }
        protected override bool ShouldHandle(Item item)
        {
            return item.Name.StartsWith(Name);
        }
    }
}
