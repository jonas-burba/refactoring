﻿namespace GildedRose.Console.Handlers
{
    public class BrieHandler : BaseHandler
    {
        public BrieHandler(BaseHandler nextHandler) : base(nextHandler)
        {
        }

        protected override string Name => "Aged Brie";
        protected override int BaseQualityChangeRate => 1;

        protected override int QualityChangeRate(int sellIn)
        {
            return BaseQualityChangeRate;
        }
    }
}
