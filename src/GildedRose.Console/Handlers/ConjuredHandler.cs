﻿using GildedRose.Entity;

namespace GildedRose.Console.Handlers
{
    public class ConjuredHandler : BaseHandler
    {
        public ConjuredHandler(BaseHandler nextHandler) : base(nextHandler)
        {
        }

        protected override string Name => "Conjured ";
        protected override int BaseQualityChangeRate => 2 * base.BaseQualityChangeRate;

        protected override bool ShouldHandle(Item item)
        {
            return item.Name.StartsWith(Name);
        }
    }
}
