using GildedRose.Console.Handlers;
using GildedRose.Entity;
using Xunit;

namespace GildedRose.Tests
{
    public class TestAssemblyTests
    {
        private void Loop(BaseHandler handler, Item item, int loops)
        {
            for (int i = 0; i < loops; i++)
            {
                handler.ChangeQualityAndSellIn(item);
            }
        }

        private BaseHandler BaseHandler => new BaseHandler(null);

        private void ValidatePassthrough(BaseHandler handler)
        {
            var item = new Item { Name = "+5 Dexterity Vest", SellIn = 10, Quality = 20 };

            Loop(handler, item, 1);

            Assert.Equal(19, item.Quality);
            Assert.Equal(9, item.SellIn);

            Loop(handler, item, 9);

            Assert.Equal(10, item.Quality);
            Assert.Equal(0, item.SellIn);

            Loop(handler, item, 1);

            Assert.Equal(9, item.Quality);
            Assert.Equal(-1, item.SellIn);

            Loop(handler, item, 5);

            Assert.Equal(0, item.Quality);
            Assert.Equal(-6, item.SellIn);

            Loop(handler, item, 1);

            Assert.Equal(0, item.Quality);
            Assert.Equal(-7, item.SellIn);
        }

        [Fact]
        public void TestSulfurasHandler()
        {
            var item = new Item {Name = "Sulfuras, Hand of Ragnaros", SellIn = 0, Quality = 80};
            SulfurasHandler handler = new SulfurasHandler(BaseHandler);

            Loop(handler, item, 1);

            Assert.Equal(80, item.Quality);
            Assert.Equal(0, item.SellIn);

            ValidatePassthrough(handler);
        }

        [Fact]
        public void TestBrieHandler()
        {
            var item = new Item { Name = "Aged Brie", SellIn = 2, Quality = 0 };
            var handler = new BrieHandler(BaseHandler);

            Loop(handler, item, 1);

            Assert.Equal(1, item.Quality);
            Assert.Equal(1, item.SellIn);

            Loop(handler, item, 52);

            Assert.Equal(50, item.Quality);
            Assert.Equal(-51, item.SellIn);

            ValidatePassthrough(handler);
        }

        [Fact]
        public void TestTicketHandler()
        {
            var item = new Item
            {
                Name = "Backstage passes to a TAFKAL80ETC concert",
                SellIn = 15,
                Quality = 20
            };
            var handler = new TicketsHandler(BaseHandler);

            Loop(handler, item, 5);

            Assert.Equal(25, item.Quality);
            Assert.Equal(10, item.SellIn);

            Loop(handler, item, 5);

            Assert.Equal(35, item.Quality);
            Assert.Equal(5, item.SellIn);

            Loop(handler, item, 5);

            Assert.Equal(50, item.Quality);
            Assert.Equal(0, item.SellIn);

            Loop(handler, item, 1);

            Assert.Equal(0, item.Quality);
            Assert.Equal(-1, item.SellIn);

            ValidatePassthrough(handler);
        }


        [Fact]
        public void TestConjuredHandler()
        {
            var item = new Item { Name = "Conjured Mana Cake", SellIn = 3, Quality = 6 };
            var handler = new ConjuredHandler(BaseHandler);

            Loop(handler, item, 1);

            Assert.Equal(4, item.Quality);
            Assert.Equal(2,item.SellIn);

            Loop(handler, item, 3);
            
            Assert.Equal(0,item.Quality);
            Assert.Equal(-1,item.SellIn);

            ValidatePassthrough(handler);
        }

        [Fact]
        public void TestBaseHandler()
        {
            ValidatePassthrough(BaseHandler);
        }
    }
}